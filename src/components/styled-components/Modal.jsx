import styled from 'styled-components';

export const ModalContainer = styled.div`
  display: flex;
  flex-direction: column;
  position: fixed;
  z-index: 1000;
  top: 50%;
  left: 50%;
  min-width: 360px;
  min-height: 100px;
  transform: translate(-50%, -50%);
  box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
  transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
  border-radius: 8px;
  font-size: 14px;
  &:hover {
    box-shadow: 0 16px 32px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
  }
`;

export const ModalHeader = styled.div`
  margin: 0;
  border-bottom: 1px solid #eee;
  position: relative;

  h2 {
    text-align: left;
    font-size: 16px;
    padding: 16px;
    margin: 0;
  }
`;

export const ModalContent = styled.div`
  flex: 1;
  padding: 8px 16px;
  margin: 0;
  text-align: left;
  display: flex;
  flex-direction: column;

  .center {
    margin: auto;
  }

  p,
  label {
    strong {
      margin: auto 4px;
    }

    &.error {
      font-size: 12px;
      padding: 8px auto;
    }
  }

  ul.limited-list {
    border: 1px solid grey;
    max-height: 80px;
    overflow-y: auto;
    padding-inline-start: 16px;
    color: #216298;
    li {
      list-style-type: none;
    }
  }
`;

export const ModalFooter = styled.div`
  padding: 16px;
  text-align: left;
  display: flex;
  justify-content: flex-end;

  button {
    margin-left: 8px;
  }
`;
