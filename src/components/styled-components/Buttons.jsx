import styled from 'styled-components';

export const Button = styled.button`
  cursor: pointer;
  font-weight: 600;
  padding: 8px 32px;
  border-radius: 4px;
  outline: none;
  &.primary {
    border: none;
    color: #216298;
    background-color: #88c4f5;
  }
  &.outline {
    border: 1px solid #216298;
    color: #216298;
    background-color: #ffffff;
  }
`;
