import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 10px;
  border: 1px solid #000;
  padding: 20px;
  
  span {
    display: block;
    margin-bottom: 5px;
  }
`;

const UserInfo = styled.div`
  border-right: 1px solid #000;
  text-align: left;
  width: 260px;
`;

const Users = styled.div`
  max-height: 300px;
  overflow: scroll;
  margin-top: 15px;
`;

export default function UserListFC() {
  const [data, setData] = useState([]);
  const [filter, setFilter] = useState('');
  const [value, setValue] = useState('');
  const [timeoutDebounce, setTimeoutDebounce] = useState(null);

  const fetchData = (username) => {
    fetch(`https://jsonplaceholder.typicode.com/users${username ? `?username=${encodeURIComponent(username)}` : ''}`).then(async (response) => {
      const result = await response.json();
      setData(result);
    });
  };

  const debounce = (func, wait = 5000) => {
    const cleanup = () => {
      if (timeoutDebounce) clearTimeout(timeoutDebounce);
    };

    return () => {
      cleanup();

      const timeout = setTimeout(func, wait);
      setTimeoutDebounce(timeout);
    };
  };


  const filterHandler = (e1) => {
    setValue(e1.target.value);
    const debounceFn = debounce(() => {
      setFilter(e1.target.value);
    });

    debounceFn();
  };

  useEffect(() => {
    fetchData(filter);
  }, [filter]);


  return (
    <div>
      <div>
        Filter:
        <input
          type="text"
          onChange={filterHandler}
          value={value}
          placeholder="Enter username"
        />
      </div>
      <Users>
        {data.map((user) => (
          <Row key={user.id}>
            <UserInfo>
              <span>{`Name: ${user.name}`}</span>
              <span>{`Username: ${user.username}`}</span>
            </UserInfo>
            <div>
              <div>
                <span>{user.address.street}</span>
                <span>{user.address.suite}</span>
                <span>{user.address.city}</span>
                <span>{user.address.zipcode}</span>
              </div>
              <div>
                <span>{user.email}</span>
                <span>{user.phone}</span>
              </div>
            </div>
          </Row>
        ))}
      </Users>
    </div>
  );
}
