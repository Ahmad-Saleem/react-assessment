/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import {
  ModalContainer,
  ModalHeader,
  ModalContent,
  ModalFooter,
} from '@components/styled-components/Modal';
import { Button } from '../styled-components/Buttons';
import Modal from '../tasktwo/Modal';

export default function DeleteReportModal(props) {
  const {
    isOpen, onClose, reportName, historyData
  } = props;
  const [confirmDelete, setConfirmed] = useState('');
  const [error, setError] = useState(false);

  return isOpen ? (
    <Modal clickOutside={onClose}>
      <ModalContainer>
        <ModalHeader>
          <h2>Are you sure you want to delete this report and its history?</h2>
        </ModalHeader>
        <ModalContent>
          <p>
            if you delete the
            <strong>{reportName}</strong>
            report, you will also delete the associated history:
          </p>
          <ul className="limited-list">
            {
              // eslint-disable-next-line react/prop-types
              historyData?.map((data) => (
                <li key={data?.id}>{data?.label}</li>
              ))
            }
          </ul>
          <label htmlFor="confirm-delete">
            Please type the word &apos;Delete&apos; to remove the
            <strong>{reportName}</strong>
            report and associated history:
            <input
              type="text"
              id="confirm-delete"
              name="confirmDelete"
              value={confirmDelete}
              onChange={({ target }) => setConfirmed(target.value)}
              className={error && 'error'}
              required
            />
          </label>
        </ModalContent>
        <ModalFooter>
          <Button
            className="primary"
            onClick={() => {
              if (confirmDelete === 'Delete') {
                setError(false);
                onClose();
              } else {
                setError(true);
              }
            }}
          >
            Delete all
          </Button>
          <Button className="outline" onClick={() => onClose()}>
            Cancel
          </Button>
        </ModalFooter>
      </ModalContainer>
    </Modal>
  ) : null;
}
