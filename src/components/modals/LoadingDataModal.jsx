import React from 'react';
import {
  ModalContainer,
  ModalContent,
} from '@components/styled-components/Modal';
import Modal from '../tasktwo/Modal';

// eslint-disable-next-line react/prop-types
export default function LoadingDataModal({ isOpen, onClose }) {
  return isOpen ? (
    <Modal clickOutside={onClose}>
      <ModalContainer>
        <ModalContent>
          <div className="flex-column center">
            <img
              src="/images/loading.svg"
              alt="loading"
              width="100"
              height="100"
            />
            <div className="center">Data is loading</div>
          </div>
        </ModalContent>
      </ModalContainer>
    </Modal>
  ) : null;
}
