import React from 'react';
import {
  ModalContainer,
  ModalHeader,
  ModalContent,
  ModalFooter,
} from '@components/styled-components/Modal';
import { Button } from '../styled-components/Buttons';
import Modal from '../tasktwo/Modal';

// eslint-disable-next-line react/prop-types
export default function DeleteFilesModal({ isOpen, onClose }) {
  return isOpen ? (
    <Modal clickOutside={onClose}>
      <ModalContainer>
        <ModalHeader>
          <h2>Are you sure you want to delete all of your files?</h2>
        </ModalHeader>
        <ModalContent>
          <p>This action can not be undone.</p>
        </ModalContent>
        <ModalFooter>
          <Button className="primary" onClick={() => onClose()}>
            Yes
          </Button>
          <Button className="outline" onClick={() => onClose()}>
            No
          </Button>
        </ModalFooter>
      </ModalContainer>
    </Modal>
  ) : null;
}
