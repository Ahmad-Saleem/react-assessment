import React from 'react';
import useDisclosure from '../../hooks/useDisclosure';
import DeleteFilesModal from '../modals/DeleteFilesModal';
import DeleteReportModal from '../modals/DeleteReportModal';
import LoadingDataModal from '../modals/LoadingDataModal';

export default function ModalsDemo() {
  const {
    onOpen: onOpen1,
    isOpen: isOpen1,
    onClose: onClose1,
  } = useDisclosure();
  const {
    onOpen: onOpen2,
    isOpen: isOpen2,
    onClose: onClose2,
  } = useDisclosure();
  const {
    onOpen: onOpen3,
    isOpen: isOpen3,
    onClose: onClose3,
  } = useDisclosure();
  return (
    <div>
      <button type="button" onClick={() => onOpen1()}>
        Data loading modal
      </button>
      <button type="button" onClick={() => onOpen2()}>
        Delete files modal
      </button>
      <button type="button" onClick={() => onOpen3()}>
        Delete report and history modal
      </button>

      <LoadingDataModal onClose={onClose1} isOpen={isOpen1} />
      <DeleteFilesModal onClose={onClose2} isOpen={isOpen2} />
      <DeleteReportModal
        onClose={onClose3}
        isOpen={isOpen3}
        reportName="Executive Metrics"
        historyData={[
          {
            id: 1,
            label: 'January 2020',
          },
          {
            id: 1,
            label: 'February 2020',
          },
          {
            id: 1,
            label: 'March 2020',
          },
          {
            id: 1,
            label: 'April 2020',
          },
          {
            id: 1,
            label: 'May 2020',
          },
          {
            id: 1,
            label: 'June 2020',
          },
        ]}
      />
    </div>
  );
}
