import React, { useCallback, useEffect, useRef } from 'react';
import ReactDOM from 'react-dom';

export default function Modal(props) {
  const ref = useRef();

  const clickOutside = useCallback(
    (e) => {
      if (ref && !ref.current.contains(e.target)) {
        // eslint-disable-next-line no-unused-expressions
        props?.clickOutside();
      }
    },
    [props]
  );

  useEffect(() => {
    document.addEventListener('click', clickOutside);
    return () => document.removeEventListener('click', clickOutside);
  }, [clickOutside]);

  return ReactDOM.createPortal(
    <div ref={ref}>{props?.children}</div>,
    document.getElementById('modal-root')
  );
}
