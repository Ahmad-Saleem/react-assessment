import { useCallback, useState } from 'react';

export default function useDisclosure() {
  const [isOpenState, setIsOpen] = useState(false);

  const onOpen = useCallback(() => {
    setIsOpen(true);
  }, []);

  const onClose = useCallback(() => {
    setIsOpen(false);
  }, []);

  return { isOpen: !!isOpenState, onOpen, onClose };
}
